##################################################
## Simple network scanner using 'scapy' library ##
##################################################

###########################################################################################################
### DODAC usprawnienia:
# - mozliwosc spoofowania MAC adresu
# - mozliwosc spoofowania adresu IP (zmiana adresu IP w wysylanym pakiecie)
# - mozliwosc zmiany zrodlowego portu TCP
#
# - wykorzystac biblioteke 'click' do obslugi argumentow w CLI: https://click.palletsprojects.com/en/8.1.x/
# - zrobi refactoring kodu (poprawi czytelnosc, uproscic co sie da)
#
###########################################################################################################


from scapy.all import *
import sys


# config
conf.verb = 0  # reduces verbosity of responses from scapy


def load_ports_from_file(filename):
    ports_file = open(filename)
    ports = ports_file.read().split(',')
    return [int(port) for port in ports]

def is_ping_reply(ping):
    return ping[1][ICMP].type == 0  # response code - '0' is a valid host response to ping (ICPM) request

def is_tcp_synack(packet):
    return packet[1][TCP].flags == "SA"


if len(sys.argv) != 2:
    print("Skrypt przyjmuje dokadnie dwa argumenty")
    print(f"python3 {sys.argv[0]} <adres sieci lub hosta>")

target = sys.argv[1]  # network or host address given as an argument when starting script

print("[+] Stage: Host discovery")

pings, unanswered = sr(IP(dst=target)/ICMP(), timeout=2)

hosts = []

for ping in pings:
    if not is_ping_reply(ping):
        continue

    hosts.append(
        {
            "ip": ping[0].dst,
            "services": []
        }
    )

print("[+] Stage: Service discovery")

nmap_top1000_int = load_ports_from_file("./nmap-top1000.txt")

for host in hosts:
    tcp_results, unanswered = sr(IP(dst=host['ip'])/TCP(dport=nmap_top1000_int), timeout=2)
    print(f"Host: {host['ip']}")
    for tcp_conn in tcp_results:
        if not is_tcp_synack(tcp_conn):
            continue
        host["services"].append(tcp_conn[0][TCP].dport)
        print(f"\t- Open port: {tcp_conn[0][TCP].dport}")