# Cracking MD5 hash using 'hashlib' library and list of passwords from Kali linux


import hashlib


target_hash = "9f6e6800cfae7749eb6c486619254b9c"
wordlist = open("john.lst")
passwords = wordlist.readlines()

for password in passwords:
    if password[0] == "#":
        continue
    password = password.strip()
    hash_md5 = hashlib.md5(password.encode("UTF-8")).hexdigest()

    if hash_md5 == target_hash:
        print(f"Password found! It's: {password}")