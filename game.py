# Simple number guessing game

import random


number = random.randrange(1, 10)
guess = 0

while guess != number:
    guess = int(input("Guess number: "))
    if guess > number:
        print("Too high!")
    elif guess < number:
        print("Too low!")
    else:
        print("CONGRATULATIONS!!")