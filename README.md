# SEKURAK Workshops

This repo contains a source code produced during a workshop called "Czy Pythonem można hackować wszystko?"
which I participated in. It was hosted by Mateusz Lewczak from SEKURAK.PL / SECURITUM.PL 
Info about the workshop can be found here: https://sklep.securitum.pl/python-hacking

# Agenda

1. Introduction to Python programming using Jupyter.  
a. You will learn the syntax of Python.  
b. You will learn what variables are and how to perform operations on them.  
c. You will delve into conditional expressions, loops, and functions.  
d. We will work with files.  
e. You will learn best practices.  


2. Creating useful security testing tools.  
a. We will discuss web scraping with Requests vs Selenium.  
b. We will build a simple network scanner and vulnerability scanner using the Scapy and Selenium libraries.  
c. We will automate data exfiltration using Blind SQL Injection vulnerabilities.  
d. We will expedite the exploitation of Insecure Deserialization in Java.  
e. We will launch an attack on a custom hashing algorithm from a REAL pentest!  
f. We will discuss the Dependency Confusion attack in the PIP package manager.  